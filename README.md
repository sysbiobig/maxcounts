# Maxcounts

*Maxcounts* is a novel approach to compute genomic feature (i.e. exon, gene, transcript) counts from RNA-seq reads aligned on a reference genome ([Finotello at al., 2014](http://www.biomedcentral.com/1471-2105/15/S1/S7)). Once reads have been aligned to a genomic feature, using any read alignment tool, maxcounts quantify exon/gene/transcript expression as the maximum of its positional counts (i.e. the number of reads covering each base of the genomic feature). 

Maxcounts is available as a patch to the [bedtools suite](http://bedtools.readthedocs.org/en/latest/).


## Installation

1) Download and extract bedtools v2.19.1

```
$ wget https://github.com/arq5x/bedtools2/releases/download/v2.19.1/bedtools-2.19.1.tar.gz
$ tar -zxf bedtools-2.19.1.tar.gz
```

2) Put `maxcounts.patch` on the same directory where you extracted the archive:

```
$ ls
bedtools2-2.19.1  maxcounts.patch
```

3) Apply the patch to bedtools

```
$ cd bedtools2-2.19.1
$ patch -p1 < ../maxcounts.patch
```

4) Compile (patched) bedtools

```
make
```



## Usage

*Maxcounts* can be computed using the [coverageBed](http://bedtools.readthedocs.org/en/latest/content/tools/coverage.html?highlight=coveragebed) function of the [bedtools suite](http://bedtools.readthedocs.org/en/latest/), by selecting `-max` option.

The following instruction computes `maxcounts` over exons listed in `annot.gff`, considering read coverage summarized in `alignments.bam` (BAM file of reads aligned over a reference genome by any alignment tool): 

```
coverageBed -max -abam alignments.bam -b annot.gff > maxcounts 
```

Alternatively, `-maxm M` option allows to compute `maxcounts` as the median read coverage across the exon bases having the highest coverage. When `M` is an integer, `K = min(M,L)` bases are considered, where `L` is exon length. When `M` is a fraction between 0 and 1, the first `M · L` bases are used.

The following instruction computes maxcounts, on each exon, as the median of its first 20% bases with the highest coverages: 

```
coverageBed -maxm 0.2 -abam alignments.bam -b annot.gff > maxcounts
```

*Maxcounts* is distributed along with `formatCounts.sh`, a bash script for creating a matrix of counts starting from multiple files generated with coverageBed (options: `-counts`, `-max`, `-maxm`). For see its standard usage, use the bash command: 

```
 ./formatCounts.sh 
```



## Advanced usage



## References

Finotello, F., Lavezzo, E., Bianco, L., Barzon, L., Mazzon, P., Fontana, P., Toppo, S., Di Camillo, B., *Reducing bias in RNA sequencing data: a novel approach to compute counts.* **BMC Bioinformatics** 15, S7 (2014). [https://doi.org/10.1186/1471-2105-15-S1-S7](https://doi.org/10.1186/1471-2105-15-S1-S7)


## Software authors
Francesca Finotello, Paolo E. Mazzon, Giacomo Baruzzo


## Software license
All the codes are freely available under a GNU Public License (Version 2). 




